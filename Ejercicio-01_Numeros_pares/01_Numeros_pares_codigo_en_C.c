#include <stdio.h>

int i=0;

int main() {
	
	printf("Numeros pares de 0 a 100\n\n");
	
	for(i=0;i<=100;i++){
		if((i%2)==0){
			printf("%i\n",i);
		}
	}
	
	printf("\nPulsa una tecla para salir\n");
	
	getchar();
	return 0;
}

/*
//Problema: Elabora un programa que reciba como entrada un monto en d�lares (USD) y devuelva la cantidad equivalente en pesos mexicanos (MXN)
//Crea un subdirectorio en tu repositorio con el nombre "ejercicio-03_dolares_pesos"
// Convension guion_bajo
*/


#include <windows.h>/*Necesaria para funcion "efecto_procesar()"*/
#include <stdio.h>

float cantidad_dolares    = 0.0f;
float cantidad_pesos      = 0.0f;

float relacion_dolar_peso = 22.37f;
char fecha [] ="12/08/2020";
	
int main() {
	
	printf("Programa que recibe como entrada un monto en dolares (USD) y \ndevuelve la cantidad equivalente en pesos mexicanos (MXN)\n");
	
	printf("\nIngrese una cantidad (USD) para convertir a pesos mexicanos (MXN)\n");
	scanf("%f",&cantidad_dolares);
	
	cantidad_pesos = cantidad_dolares * relacion_dolar_peso;
		
	printf("\n\n\t%.2f dolares (USD) equivale a %.2f pesos mexicanos (MXN)\n\n\t*Teniendo en cuenta que la relacion dolar-peso para \n\tla fecha %s es de %.2f \n",cantidad_dolares,cantidad_pesos,fecha,relacion_dolar_peso);

	/*Metodo de espera para que la consola no cierre inmediatamente, una vez que arroja el resultado*/
	printf("\n\n - Pulsa una tecla para salir\n");
	
	getch();
	return 0;
}

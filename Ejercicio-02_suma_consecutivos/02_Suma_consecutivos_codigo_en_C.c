/*
//Problema: Elabora un programa que reciba un n�mero entre 1 y 50 y devuelva la suma de los n�meros consecutivos del 1 hasta ese n�mero.
//
//	Crea un subdirectorio en tu repositorio con el nombre "ejercicio-02_suma_consecutivos"
// Convension guion_bajo
*/
#include <windows.h>/*Necesaria para funcion "efecto_procesar()"*/
#include <stdio.h>

int numero_entrada=1; /*//variable de entrada, ingrsada por el usuario*/

int sumatoria=0; /*variable contenedor*/
int i=0; /*variable iteradora*/

int main() {
	
	printf("Programa que obtiene la suma de los numeros consecutivos del 1 hasta un numero de entrada\n");
	
	/*//El ciclo do se repite hasta que el usuario ingrese un numero valido segun las caracteristicas del problema planteado en la parte de arriba*/
	do{
		printf("Ingrese un numero entero entre 1 y 50\n");
		scanf("%i",&numero_entrada);
		printf("El numero introducido fue: %i\n",numero_entrada);
		if((numero_entrada<=0) || (numero_entrada>50)){
			printf("Error. Introduce un numero valido \n\n\n");
		}
	}
	while ((numero_entrada<=0) || (numero_entrada>50));
	
		printf("\n\n\n\n\tTu numero es valido !!!\n\n\n");
		Sleep(500);
		printf("  Llamando a Funcion para: \n  Obtener la suma de los numeros consecutivos del 1 hasta el numero %i\n",numero_entrada);	
		
		/*efecto de procesar*/
		printf("\n_ ");
		Sleep(350);
		printf("Procesando ");
		efecto_procesar();
		
		/*Se manda a llamar la funcion que realiza la suma de numeros consecutivos*/
		printf("\n");
		suma_consecutiva(numero_entrada);
		
		/*Metodo de espera para que la consola no cierre inmediatamente, una vez que arroja el resultado*/
		printf("\n\n - Pulsa una tecla para salir\n");
		
		getch();
		return 0;
}


int suma_consecutiva (int numero){
	for(i=1;i<=numero;i++){
		printf("\n\t%i + %i = ",i,sumatoria);
		sumatoria=sumatoria+i;
		printf("%i",sumatoria);
	}
	printf("\n\n\tLa suma de los numeros consecutivos de 1 hasta %i es = %i",numero,sumatoria);
}

int efecto_procesar(){
	int delay=50;
	delay=400;
	for(i=0;i<50;i++){
		if(delay<=0){
			Sleep(10);
		}
		else{
			Sleep(delay);	
		}
		if(i>2){
			delay=delay-50;
		}
		printf(".");
	}
}



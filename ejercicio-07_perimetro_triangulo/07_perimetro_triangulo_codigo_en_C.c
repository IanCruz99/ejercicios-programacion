#include <stdio.h>

float lado_a      = 0.0f;
float lado_b     = 0.0f;
float lado_c     = 0.0f;
float perimetro = 0.0f;

int i = 0;
char seleccion_triangulo = '0';

int main() {
	
	printf("Programa que calcula el perimetro de un triangulo.\n\n\n\n"); 
	
	printf("Existen 3 tipos de triangulo, los cuales son: equilatero, isosceles, escaleno.\n\n"); 
	
	printf("1 : El triangulo equilatero consta de 3 lados iguales.\n\n");	 
	
	printf("2 : El triangulo isosceles se conforma de 2 lados iguales y uno diferente.\n\n");	 
	
	printf("3 : El triangulo escaleno se conforma de 3 lados distintos, \nlo cual implica que todos los lados tienen un valor diferente respecto a cualquier otro lado.\n\n");	 
	
	
	printf("\n\nIntroduzca un valor para seleccionar el tipo de triangulo del cual quiere obtener el perimetro.\n\n"); 
	
	scanf("%c",&seleccion_triangulo);
	
	
	for(i=0 ; i<10 ; i++){
	printf("\n");
	}
	
	switch(seleccion_triangulo) {
	
	case '1': 
		printf("****************************************************************\n"); 
		printf("Programa que calcula el perimetro de un triangulo equilatero.\n"); 
		printf("El triangulo equilatero consta de 3 lados iguales.\n");	 
		printf("\nIngrese el valor de uno de los lados del triangulo equilatero.\n");
		scanf("%f",&lado_a);
		lado_b = lado_a;
		lado_c = lado_a;
		
		perimetro=lado_a*3.0f;
		printf("\n\n\tEl perimetro del triangulo equilatero es de %.2f [unidades], \npara el triangulo cuyos lados tienen un valor de %.2f [unidades]",perimetro,lado_a);
		
	break;
	
	case '2':
		printf("****************************************************************\n"); 
		printf("Programa que calcula el perimetro de un triangulo isosceles.\n\n"); 
		printf("El triangulo isosceles se conforma de 2 lados iguales y uno diferente.\n\n");	 
		printf("\nIngrese el valor de los lados que son iguales del triangulo isosceles.\n");
		scanf("%f",&lado_a);
		lado_b = lado_a;
		printf("\nIngrese el valor del lado restante del triangulo isosceles.\n");
		scanf("%f",&lado_c);
		
		perimetro=(lado_a + lado_b + lado_c);
		printf("\n\n\tEl perimetro de un triangulo isosceles es de de: %.2f [unidades] \nteniendo en cuenta que dos de sus lados tienen un valor de %.2f [unidades] y\nsu lado distinto cuenta con un valor de %.2f [unidades] \n",perimetro,lado_a,lado_c);
		
	break;
	
	
	case'3':
		printf("****************************************************************\n"); 
		printf("Programa que calcula el perimetro de un triangulo escaleno.\n\n"); 
		printf("El triangulo escaleno se conforma de 3 lados distintos, \nlo cual implica que todos los lados tienen un valor diferente respecto a cualquier otro lado.\n");	 
		printf("\nIngrese el valor del primer lado del triangulo escaleno.\n");
		scanf("%f",&lado_a);
		printf("\nIngrese el valor del segundo lado del triangulo escaleno.\n");
		scanf("%f",&lado_b);
		printf("\nIngrese el valor del tercer lado del triangulo escaleno.\n");
		scanf("%f",&lado_c);
		
		perimetro=(lado_a + lado_b + lado_c);
		printf("\n\n\tEl perimetro de un triangulo escaleno, \nel cual tiene tres lados de disntinto valor los cuales son: \n\t\t %.2f [unidades], \n\t\t %.2f [unidades], \n\t\t %.2f [unidades] \nda como resultado un perimietro de: %.2f [unidades]",lado_a,lado_b,lado_c,perimetro);
		
		
	break;
	
	default: 
		break;
	
	}
	
	
	printf("\n\n\tPulse una tecla para salir");
	
	getch();
	return 0;
}


Inicio

	Numerico lado_a    = 0
	Numerico lado_b    = 0
	Numerico lado_c    = 0
	Numerico perimetro = 0

	Escribe: Programa que calcula el perímetro de un triángulo isosceles. 
	Escribe: El triangulo isosceles consta de 2 lados iguales. 
	Escribe: Ingrese el valor de los lados iguales del triangulo isosceles.
	Guarda el valor ingresado en la variable "lado_a"
	Replica el valor de "lado_a" en la variable "lado_b"
	Escribe: Ingrese el valor del lado restante del triangulo isosceles.
	Guarda el valor ingresado en la variable "lado_c"

	Realiza la operacion (lado_a + lado_b + lado_c) y guarda el valor en perimetro

	Escribe: El perimetro de un triangulo isosceles, 
		el cual tiene dos lados iguales con valor de "lado_a" y
		un lado distinto con valor de "lado_c",
		da como resultado un perimietro de: "perimetro"


Fin

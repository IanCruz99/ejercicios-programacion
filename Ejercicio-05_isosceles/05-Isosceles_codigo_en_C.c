#include <stdio.h>

float lado_a      = 0.0f;
float lado_b     = 0.0f;
float lado_c     = 0.0f;
float perimetro = 0.0f;

int main() {
	
	printf("Programa que calcula el perimetro de un triangulo isosceles.\n\n"); 
	printf("El triangulo isosceles se conforma de 2 lados iguales y uno diferente.\n\n");	 
	printf("\nIngrese el valor de los lados que son iguales del triangulo isosceles.\n");
	scanf("%f",&lado_a);
	lado_b = lado_a;
	printf("\nIngrese el valor del lado restante del triangulo isosceles.\n");
	scanf("%f",&lado_c);
	
	perimetro=(lado_a + lado_b + lado_c);
	
	printf("\n\n\tEl perimetro de un triangulo isosceles, \nel cual tiene dos lados iguales con valor de %.2f [unidades] y 	\nun lado distinto con valor de %.2f [unidades], \nda como resultado un perimietro de: %.2f [unidades]",lado_a,lado_c,perimetro);
	
	getch();
	return 0;
}
	

#include <stdio.h>

float lado_a      = 0.0f;
float lado_b     = 0.0f;
float lado_c     = 0.0f;
float perimetro = 0.0f;

int main() {
	
	printf("Programa que calcula el perimetro de un triangulo escaleno.\n\n"); 
	printf("El triangulo escaleno se conforma de 3 lados distintos, \nlo cual implica que todos los lados tienen un valor diferente respecto a cualquier otro lado.\n");	 
	printf("\nIngrese el valor del primer lado del triangulo escaleno.\n");
	scanf("%f",&lado_a);
	printf("\nIngrese el valor del segundo lado del triangulo escaleno.\n");
	scanf("%f",&lado_b);
	printf("\nIngrese el valor del tercer lado del triangulo escaleno.\n");
	scanf("%f",&lado_c);
	
	perimetro=(lado_a + lado_b + lado_c);
	
	printf("\n\n\tEl perimetro de un triangulo escaleno, \nel cual tiene tres lados de disntinto valor los cuales son: \n\t\t %.2f [unidades], \n\t\t %.2f [unidades], \n\t\t %.2f [unidades] \nda como resultado un perimietro de: %.2f [unidades]",lado_a,lado_b,lado_c,perimetro);
	
	getch();
	return 0;
}

